package com.systemdk.springbootbackendangular.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "roles")
public class Rol implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cod_rol")
    private Long codRol;

    @Column(name = "nombre", unique = true, length = 20)
    private String nombre;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codRol")
    private List<UsuarioRol> UsuarioRolList;

}
