package com.systemdk.springbootbackendangular.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "usuarios")
public class Usuario implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cod_usuario")
    private Long codUsuario;

    @Column(name = "username", unique = true, length = 20)
    private String username;

    @Column(name = "password", length = 60)
    private String password;

    @Column(name = "password_encrypt")
    private String passwordEncrypt;

    @Column(name = "es_activo")
    private boolean esActivo;

    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL, mappedBy = "codUsuario")
    private List<UsuarioRol> UsuarioRolList;

}
