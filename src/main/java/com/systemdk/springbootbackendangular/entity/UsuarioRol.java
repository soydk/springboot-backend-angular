package com.systemdk.springbootbackendangular.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="usuarios_roles", uniqueConstraints = {@UniqueConstraint(columnNames = {"cod_usuario", "cod_rol"})})
public class UsuarioRol implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codUsuarioRol;

    @ManyToOne
    @JoinColumn(name = "cod_usuario")
    private Usuario codUsuario;

    @ManyToOne
    @JoinColumn(name = "cod_rol")
    private Rol codRol;

}
