package com.systemdk.springbootbackendangular.dao;

import com.systemdk.springbootbackendangular.entity.Rol;
import com.systemdk.springbootbackendangular.entity.Usuario;
import com.systemdk.springbootbackendangular.entity.UsuarioRol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IRolDao extends JpaRepository<Rol, Long> {

    @Query("select ur FROM UsuarioRol ur where ur.codUsuario.codUsuario= ?1")
    List<UsuarioRol> buscarUserRolByUsuario(Long usuario);

}
