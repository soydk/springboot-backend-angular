package com.systemdk.springbootbackendangular.dao;

import com.systemdk.springbootbackendangular.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface IUsuarioDao extends JpaRepository<Usuario, Long> {

    Usuario findByUsername(String username);

    @Query("select u from Usuario u where u.username = ?1")
    Usuario buscarPorUsername(String username);

}
