package com.systemdk.springbootbackendangular.dao;

import com.systemdk.springbootbackendangular.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IRegionDao extends JpaRepository<Region, Long> {

    //solo como ejemplo esta es una consulta JPA
    @Query("select r from Region r")
    List<Region> findAllRegionesJPA();

}
