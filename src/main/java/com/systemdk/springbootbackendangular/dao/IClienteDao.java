package com.systemdk.springbootbackendangular.dao;

import com.systemdk.springbootbackendangular.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.systemdk.springbootbackendangular.entity.Cliente;

import java.util.List;

public interface IClienteDao extends JpaRepository<Cliente, Long>{

    @Query("select r from Region r")
    List<Region> findAllRegiones();
    
}
