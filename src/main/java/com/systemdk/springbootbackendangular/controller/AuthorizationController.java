package com.systemdk.springbootbackendangular.controller;

import com.systemdk.springbootbackendangular.dto.AuthenticationResponse;
import com.systemdk.springbootbackendangular.dto.LoginForm;
import com.systemdk.springbootbackendangular.dto.RegisterForm;
import com.systemdk.springbootbackendangular.service.AuthorizationService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = {"*"})
@RestController
@RequestMapping("/api/auth")
public class AuthorizationController {

    private final AuthorizationService authorizationService;

    public AuthorizationController(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @Secured({"ROLE_ADMIN","ROLE_USER"})
    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> login(@RequestBody LoginForm loginDTO){
        AuthenticationResponse response = authorizationService.loginAuthenticated(loginDTO);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/register")
    public ResponseEntity<?> registrarse(@RequestBody RegisterForm registerFormDTO){
        return ResponseEntity.ok(authorizationService.registrarUsuario(registerFormDTO));
    }

}
