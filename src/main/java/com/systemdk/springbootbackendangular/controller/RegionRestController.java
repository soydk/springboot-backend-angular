package com.systemdk.springbootbackendangular.controller;

import com.systemdk.springbootbackendangular.entity.Region;
import com.systemdk.springbootbackendangular.service.logicInterface.IRegionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = {"*"})
@RestController
@RequestMapping("/api")
public class RegionRestController {

    private final IRegionService regionService;

    private final Logger log = LoggerFactory.getLogger(RegionRestController.class);

    public RegionRestController(IRegionService regionService) {
        this.regionService = regionService;
    }

    @GetMapping("/regiones")
    public List<Region>buscarRegiones(){
        return regionService.getAllRegion();
    }

}
