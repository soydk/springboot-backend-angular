package com.systemdk.springbootbackendangular.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.systemdk.springbootbackendangular.service.logicInterface.IUploadService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.core.io.Resource;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.systemdk.springbootbackendangular.entity.Cliente;
import com.systemdk.springbootbackendangular.service.logicInterface.IClienteService;
import org.springframework.web.multipart.MultipartFile;


//si se desea que cualquier cliete pueda hacer la peticion solo se cambia la direccion http://localhost:4200 por "*"
@CrossOrigin(origins = {"*"})
//CrossOrigin especifica la ip o dominio al cual va a tener acceso a mis endpoints, es masomenos como darle seguridad para que solo la aplicacion acceda a los metodos de obtencion o insersion de datos
@RestController
@RequestMapping("/api")
public class ClienteRestController {

    public final IClienteService clienteService;
    public final IUploadService uploadService;


    private final Logger log = LoggerFactory.getLogger(ClienteRestController.class);

    public ClienteRestController(IClienteService clienteService, IUploadService uploadService) {
        this.clienteService = clienteService;
        this.uploadService = uploadService;
    }

    @GetMapping("/clientes")
    public List<Cliente> findAllClients() {
        return clienteService.findAllClient();
    }

    @GetMapping("/clientes/page/{page}")
    public Page<Cliente> findAllClients(@PathVariable Integer page) {
        Pageable parameterPage = PageRequest.of(page, 4, Sort.by("id"));
        return clienteService.findAllClient(parameterPage);
    }

    @GetMapping("/clientes/pageCast/{page}")
    public List<Cliente> findAllClientPageCast(@PathVariable Integer page) {
        return clienteService.findAllClientPage(page);
    }

    @GetMapping("/clientes/{id}")
    public ResponseEntity<?> findClienteBycodigo(@PathVariable Long id) {
        Cliente cliente = null;
        Map<String, Object> response = new HashMap<>();

        try {
            cliente = clienteService.findClienteById(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar la consulta en la base de datos");
            response.put("Error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (cliente == null) {
            response.put("mensaje", "El cliente ID: ".concat(id.toString()).concat(" no existe en la base de datos!"));
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(cliente, HttpStatus.OK);
    }

    @PostMapping("/clientes/save")
    public ResponseEntity<?> saveCliente(@Valid @RequestBody Cliente cliente, BindingResult result) {
        Cliente clientenew = null;
        Map<String, Object> response = new HashMap<>();

        //verfiicamos si todos los cambios del Objeto cliente son validos
        //si no son validos se procede a crear ingresar los errores al map
        if (result.hasErrors()) {
            List<String> errores = result.getFieldErrors()
                    .stream().map(e -> "El campo " + e.getField() + " " + e.getDefaultMessage()).toList();

            response.put("Errors", errores);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            clientenew = clienteService.savCliente(cliente);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar insert en la base de datos");
            response.put("Error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("cliente", clientenew);
        response.put("mensaje", "Cliente guardado exitosamente");
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/cliente/update/{id}")
    public ResponseEntity<?> updaCliente(@Valid @RequestBody Cliente clienteAC, BindingResult result, @PathVariable Long id) {
        Cliente clienteActual = null;

        Map<String, Object> response = new HashMap<>();

        //verfiicamos si todos los cambios del Objeto cliente son validos
        //si no son validos se procede a crear ingresar los errores al map
        if (result.hasErrors()) {
            List<String> errores = result.getFieldErrors()
                    .stream().map(e -> "El campo " + e.getField() + " " + e.getDefaultMessage()).toList();

            response.put("Errors", errores);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            clienteActual = clienteService.updateClientes(clienteAC, id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar actualizacion en base de datos");
            response.put("Error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("cliente", clienteActual);
        response.put("mensaje", "Cliente actualizado exitosamente");
        return new ResponseEntity<>(response, HttpStatus.CREATED);

    }

    @DeleteMapping("/cliente/delete/{id}")
    public ResponseEntity<?> deleteCliente(@PathVariable Long id) {
        Map<String, Object> response = new HashMap<>();
        try {
            Cliente cliente = clienteService.findClienteById(id);
            uploadService.eliminar(cliente.getFoto());
            clienteService.deleteCliente(id);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al eliminar en base de datos");
            response.put("Error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "Cliente eliminado exitosamente");
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    //metodo para subir la imagenes asociadas con el cliente creado
    @PostMapping("/cliente/upload")
    public ResponseEntity<?> uploadDates(@RequestParam("archivo") MultipartFile archivo, @RequestParam("id") Long id) {
        Map<String, Object> response = new HashMap<>();

        Cliente cliente = clienteService.findClienteById(id);
        if (!archivo.isEmpty()) {

            String nombreArchivo = "";
            try {
                nombreArchivo = uploadService.copiar(archivo);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            String nombreFotoAnterior = cliente.getFoto();
            uploadService.eliminar(nombreFotoAnterior);

            cliente.setFoto(nombreArchivo);
            clienteService.savCliente(cliente);

            response.put("cliente", cliente);
            response.put("mensaje", "Has subido correctamente la imagen: " + nombreArchivo);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/download/img/{nombreFoto:.+}")
    public ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto) {

        Resource recurso = null;
        try {
            recurso = uploadService.cargar(nombreFoto);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpHeaders cabecera = new HttpHeaders();
        cabecera.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"");

        return new ResponseEntity<>(recurso, cabecera, HttpStatus.OK);
    }


}
