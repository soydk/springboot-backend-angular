package com.systemdk.springbootbackendangular.config;

import com.systemdk.springbootbackendangular.exceptions.AppException;
import com.systemdk.springbootbackendangular.exceptions.ResourceNotFoundException;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class JwtService {

    @Value("${app.jwt-secret}")
    private String secret_key;

    @Value("${app.jwt-expiration-milliseconds}")
    private int jwtExpirationInMs;

    public String extraerUsername(String jwToken) {
        return extraerClaim(jwToken, Claims::getSubject);
    }

    //recordemos que un token se confirma de 3 parte: 1. tipo Token 2. Reclamaciones y Datos del usuario, 3. Signature o tipo de encritacion

    private <T> T extraerClaim(String token, Function<Claims, T> claimsResolver){
        //obtenemos todos los claims de la funcion extraer todos los claims(reclamaciones)
        final Claims claims = extraerAllClaims(token);
        return claimsResolver.apply(claims);
    }

    //este metodo sera para generar Token sin reclamaciones adicionales, osea solo con informacion del usuario
    public String generarToken(UserDetails userDetails){
        Map<String, Object> extraClaims = new HashMap<>();
        extraClaims.put("authorities", userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()));
        extraClaims.put("isActivo", userDetails.isEnabled());
        return generateToken(extraClaims, userDetails);
    }

    //este metodo sera para generar Token con reclamaciones extra
    public String generateToken(Map<String, Object> extraReclamaciones, UserDetails userDetails){
        Date fechaActual = new Date();
        Date fechaExpiracion = new Date(fechaActual.getTime() + jwtExpirationInMs);
        return Jwts
                .builder()
                .setClaims(extraReclamaciones) //esta son reclamaciones adicionales ejemplo de metaData del token osea otros atributos adicionales
                .setSubject(userDetails.getUsername())//obtenemos el usuario
                .setIssuedAt(new Date(System.currentTimeMillis()))//definimos cuando se creo el token
                .setExpiration(fechaExpiracion)//definimos cuando expirara(24 horas)
                .signWith(getSignIngKey(), SignatureAlgorithm.HS256) //encryptamos con nuestra clave y algoritmos HS256
                .compact(); //compact nos compactara todo formandonos un string q sera nuestro Token
    }

    //este metodo nos servira para validar si el token es correcto
    //los parametros son token y userdetails: es para verificar si ese token pertenece a ese userdeails o detall de usuario
    public boolean esValidoToken(String token, UserDetails userDetails){
        try {
            final String username = extraerUsername(token);
            //preuntamos si el usuario se parece al de la sesion y si el token aun es valido
            return (username.equals(userDetails.getUsername()) && !esExpiradoToken(token));
        } catch (SignatureException ex){
            throw new AppException(HttpStatus.BAD_REQUEST, "Token JWT firma no valida");
        } catch (MalformedJwtException ex) {
            throw new AppException(HttpStatus.BAD_REQUEST, "Token JWT no valido");
        } catch (ExpiredJwtException ex) {
            throw new AppException(HttpStatus.BAD_REQUEST, "Token JWT caducado");
        } catch (UnsupportedJwtException ex) {
            throw new AppException(HttpStatus.BAD_REQUEST, "Token JWT no compatible");
        } catch (IllegalArgumentException ex) {
            throw new AppException(HttpStatus.BAD_REQUEST, "La cadena clains JWT esta vacia");
        }
    }

    private boolean esExpiradoToken(String token) {
        //sabemos si el token es valido si su fecha de expirado es antes q la actual
        return extrarExpiracionToken(token).before(new Date());
    }

    private Date extrarExpiracionToken(String token) {
        //con el claims podemos acceder a los datos(reclamos) del token
        return extraerClaim(token, Claims::getExpiration);
    }


    //esto es para traer todas las reclamaciones q conforman el token
    private Claims extraerAllClaims(String token){
        return Jwts
                .parserBuilder()
                .setSigningKey(getSignIngKey())//get signingKey es un metodo para obtener la firma del token decodificada
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    private Key getSignIngKey() {
        byte[] keyBytes = Decoders.BASE64.decode(secret_key);
        return Keys.hmacShaKeyFor(keyBytes);
    }

}
