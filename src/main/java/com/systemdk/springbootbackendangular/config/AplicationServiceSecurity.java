package com.systemdk.springbootbackendangular.config;

import com.systemdk.springbootbackendangular.dao.IRolDao;
import com.systemdk.springbootbackendangular.dao.IUsuarioDao;
import com.systemdk.springbootbackendangular.entity.Rol;
import com.systemdk.springbootbackendangular.entity.Usuario;
import com.systemdk.springbootbackendangular.entity.UsuarioRol;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Configuration
@RequiredArgsConstructor
public class AplicationServiceSecurity{

    private Logger log = LoggerFactory.getLogger(AplicationServiceSecurity.class);

    //esta clase es una implementacion de SpringSecurity para poder obtener el usuario que esta iniciando sesion con sus respectivas credenciales
    //Es el primer paso para la implementacion de Seguridades con JWT
    private final IUsuarioDao usuarioDao;
    private final IRolDao rolDao;

    @Bean
    @Transactional
    public UserDetailsService userDetailsService(){
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                Usuario usuario = usuarioDao.buscarPorUsername(username);
                usuario.setUsuarioRolList(new ArrayList<>());
                usuario.getUsuarioRolList().addAll(getRolesUser(usuario));
                if(usuario == null){
                    log.error("Error en el Login, no existe usuario: '"+username+"' en el sistema");
                    throw new UsernameNotFoundException("Error en el Login, no existe usuario: '"+username+"' en el sistema");
                }

                //una vez obtenido el usuario procedemos a crear la instancia de User(clase de springSecurity), la cual tendra q setearse los datos del usuario
                //el ultimo atributo authorities, es una coleccion que crearemos a traves de los roles q tiene el usuario
                List<GrantedAuthority> authorities = usuario.getUsuarioRolList()
                        .stream()
                        .map( r -> new SimpleGrantedAuthority(r.getCodRol().getNombre())).collect(Collectors.toList());
                return new User(usuario.getUsername(), usuario.getPassword(), usuario.isEsActivo(), true, true, true, authorities);
            }
        };
    }

    public List<UsuarioRol> getRolesUser(Usuario usuario){
        List<UsuarioRol> userRol = new ArrayList<>();
        try {
            userRol = rolDao.buscarUserRolByUsuario(usuario.getCodUsuario());
        }catch (Exception e){
            return new ArrayList<>();
        }
        return userRol;
    }

    @Bean
    public AuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration configuration) throws Exception {
        return configuration.getAuthenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
       return new BCryptPasswordEncoder();
    }

}
