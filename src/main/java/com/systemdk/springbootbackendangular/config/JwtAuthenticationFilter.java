package com.systemdk.springbootbackendangular.config;

import com.systemdk.springbootbackendangular.exceptions.AppException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JwtService jwtService;
    private final UserDetailsService userDetailsService;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        //pasamos la authorizacion a una cabecera es decir el token que se envia desde el cliente
        final String authHeader = request.getHeader("Authorization");
        //creamos variable para obtener el cuerpo del token sin el BEARER
        final String jwt;
        //Creamos una variable para extraer el usuario en este caso user podria ser el email
        final String username;

        //si el header es null o no comienza con Bearer, siginifica q no hay token, por lo tanto retornamos nada y terminamos la funcion
        if(authHeader == null || !authHeader.startsWith("Bearer")){
            filterChain.doFilter(request, response);
            return;
        }
        //si todo esta bien, extraemos todo el token
        jwt = authHeader.substring(7);//comenzamos desde el indice 7 porque desde alli la palabra Bearer queda fuera y solo se extraera el token

        //obtenemos el usuario mediante el JwtService que creamos
        username = jwtService.extraerUsername(jwt);
        //si mi usuario no es nulo y no esta authenticado
        if(username != null && SecurityContextHolder.getContext().getAuthentication() == null){
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            //verificamos la validez del token
            if(jwtService.esValidoToken(jwt, userDetails)){
                //si el token es valido procedemos a crear un authenticacion ya que mi usuario no esta autenticado, pero si es valido (quiere decir q es primera vez q ingresa sesion), asi que actualizaremos el contexto de seguridad
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authToken);//procedemos a setearle la authenticacion al contexto de seguridad por primera vez
            }
        }
        filterChain.doFilter(request, response);
    }
}
