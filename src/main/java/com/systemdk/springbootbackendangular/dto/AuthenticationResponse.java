package com.systemdk.springbootbackendangular.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationResponse {

    private String tipoDeToken = "Bearer";
    private String username;
    private String tipoOperacion;
    private String tokenDeAcceso;



}
