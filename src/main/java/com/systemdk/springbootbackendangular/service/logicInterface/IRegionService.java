package com.systemdk.springbootbackendangular.service.logicInterface;

import com.systemdk.springbootbackendangular.entity.Region;

import java.util.List;

public interface IRegionService {

    List<Region> getAllRegion();

}
