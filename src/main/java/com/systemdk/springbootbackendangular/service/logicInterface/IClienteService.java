package com.systemdk.springbootbackendangular.service.logicInterface;
import java.util.List;
import com.systemdk.springbootbackendangular.entity.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface IClienteService {
    List<Cliente> findAllClient();
    Page<Cliente> findAllClient(Pageable pageable);
    List<Cliente> findAllClientPage(Integer page);
    Cliente findClienteById(Long id);
    Cliente savCliente(Cliente cliente);
    void deleteCliente(Long id);
    Cliente updateClientes(Cliente clienteUpdate, Long id);
}
