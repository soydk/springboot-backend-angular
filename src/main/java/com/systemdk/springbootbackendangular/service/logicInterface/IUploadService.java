package com.systemdk.springbootbackendangular.service.logicInterface;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;

public interface IUploadService {
    Resource cargar(String nombreFoto) throws MalformedURLException;
    String copiar(MultipartFile archivo) throws IOException;
    boolean eliminar(String nombreFoto);
    Path getPath(String tipoDireccion, String nombreFoto);
    //buscador de rutas
    String obtenerDireccionRuta(String referencia);

}
