package com.systemdk.springbootbackendangular.service.logicImplement;


import com.systemdk.springbootbackendangular.service.logicInterface.IUploadService;
import com.systemdk.springbootbackendangular.utils.PathFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import static com.systemdk.springbootbackendangular.enums.documentPath.IMAGEN;
import static com.systemdk.springbootbackendangular.enums.documentPath.NO_IMAGEN;

@Service
public class UploadServiceImpl implements IUploadService {

    private final Logger log = LoggerFactory.getLogger(UploadServiceImpl.class);

    @Override
    public Resource cargar(String nombreFoto) throws MalformedURLException {

        Path rutaArchivo = getPath(IMAGEN.name(), nombreFoto);
        Resource recurso;

        recurso = new UrlResource(rutaArchivo.toUri());
        log.info(recurso.getFilename());

        if (!recurso.exists() && !recurso.isReadable()) {
            rutaArchivo = getPath(NO_IMAGEN.name(), "no_user.png");
            recurso = new UrlResource(rutaArchivo.toUri());
            log.info(recurso.getFilename());
        }
        return recurso;
    }

    @Override
    public String copiar(MultipartFile archivo) throws IOException {
        //con UUID estableceremos un id unico para los documentos que se vayan subiendo
        String nombreArchivo = UUID.randomUUID() + "_" + archivo.getOriginalFilename().replace(" ", "_");
        Path rutaArchivo = getPath(IMAGEN.name(), nombreArchivo);

        Files.copy(archivo.getInputStream(), rutaArchivo);

        return nombreArchivo;
    }

    @Override
    public boolean eliminar(String nombreFoto) {
        if (nombreFoto != null && nombreFoto.length() > 0) {
            Path rutaFotoAnterior = getPath(IMAGEN.name(), nombreFoto);
            File archivoAnterior = rutaFotoAnterior.toFile();
            if (archivoAnterior.exists() && archivoAnterior.canRead()) {
                archivoAnterior.delete();
                return true;
            }
        }
        return false;
    }

    @Override
    public Path getPath(String tipoDireccion, String nombreFoto) {
        return Paths.get(obtenerDireccionRuta(tipoDireccion)).resolve(nombreFoto).toAbsolutePath();
    }

    @Override
    public String obtenerDireccionRuta(String referencia) {
        PathFinder finder = new PathFinder();
        return finder.obtenerPathAbsolute(referencia);
    }
}
