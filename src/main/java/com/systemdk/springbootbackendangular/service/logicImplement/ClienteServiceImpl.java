package com.systemdk.springbootbackendangular.service.logicImplement;

import java.util.List;

import com.systemdk.springbootbackendangular.service.logicInterface.IClienteService;
import com.systemdk.springbootbackendangular.utils.PathFinder;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.systemdk.springbootbackendangular.dao.IClienteDao;
import com.systemdk.springbootbackendangular.entity.Cliente;

@Service
public class ClienteServiceImpl implements IClienteService {

    @Autowired
    private IClienteDao clienteDao;

    @Override
    public List<Cliente> findAllClient() {
        return clienteDao.findAll();
    }

    @Override
    public Page<Cliente> findAllClient(Pageable pageable) {
        return clienteDao.findAll(pageable);
    }

    @Override
    public List<Cliente> findAllClientPage(Integer page) {
        Pageable clientePageable = PageRequest.of(page, 4, Sort.by("id"));
        Page<Cliente> clientesPage= clienteDao.findAll(clientePageable);
        return clientesPage.getContent();
    }

    @Override
    public Cliente findClienteById(Long id) {
        return clienteDao.findById(id).orElse(null);
    }

    @Override
    public Cliente savCliente(Cliente cliente) {
        return clienteDao.save(cliente);
    }

    @Override
    public void deleteCliente(Long id) {
        Cliente cliente = clienteDao.findById(id).orElse(null);
        clienteDao.delete(cliente);        
    }

    public Cliente mapearBody(Cliente clienteActual, Cliente clienteUpdate){
        clienteActual.setNombre(clienteUpdate.getNombre());
        clienteActual.setApellido(clienteUpdate.getApellido());
        clienteActual.setEmail(clienteUpdate.getEmail());
        clienteActual.setRegion(clienteUpdate.getRegion());
        return clienteActual;
    }

    @Override
    public Cliente updateClientes(Cliente clienteUpdate, Long id) {
        Cliente clienteActual = findClienteById(id);
        Cliente clienteActualizar = mapearBody(clienteActual, clienteUpdate);
        return savCliente(clienteActualizar);        
    }
    
}
