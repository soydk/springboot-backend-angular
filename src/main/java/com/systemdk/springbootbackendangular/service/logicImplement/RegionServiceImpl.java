package com.systemdk.springbootbackendangular.service.logicImplement;

import com.systemdk.springbootbackendangular.dao.IRegionDao;
import com.systemdk.springbootbackendangular.entity.Region;
import com.systemdk.springbootbackendangular.service.logicInterface.IRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionServiceImpl implements IRegionService {

    @Autowired
    private IRegionDao regionDao;

    @Override
    public List<Region> getAllRegion() {
        return regionDao.findAllRegionesJPA();
    }
}
