package com.systemdk.springbootbackendangular.service;

import com.systemdk.springbootbackendangular.config.JwtService;
import com.systemdk.springbootbackendangular.dao.IRolDao;
import com.systemdk.springbootbackendangular.dao.IUsuarioDao;
import com.systemdk.springbootbackendangular.dto.AuthenticationResponse;
import com.systemdk.springbootbackendangular.dto.LoginForm;
import com.systemdk.springbootbackendangular.dto.RegisterForm;
import com.systemdk.springbootbackendangular.entity.Rol;
import com.systemdk.springbootbackendangular.entity.Usuario;
import com.systemdk.springbootbackendangular.entity.UsuarioRol;
import com.systemdk.springbootbackendangular.exceptions.AppException;
import com.systemdk.springbootbackendangular.exceptions.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthorizationService {

    private final IUsuarioDao usuarioDao;
    private final IRolDao rolDao;
    private final JwtService jwtService;
    private final PasswordEncoder passwordEncoder;
    private final UserDetailsService userDetailsService;

    private final AuthenticationManager authenticationManager;

    public AuthenticationResponse registrarUsuario(@NotNull RegisterForm registro) {
        Usuario usuario = new Usuario();
        usuario.setUsername(registro.getUsername());
        usuario.setPassword(passwordEncoder.encode(registro.getPassword()));
        usuario.setPasswordEncrypt(passwordEncoder.encode(registro.getPassword()));
        usuario.setEsActivo(true);
        usuario.setUsuarioRolList(new ArrayList<>());
        usuario.getUsuarioRolList().addAll(addUserRol(usuario));
        try {
            usuarioDao.save(usuario);
        } catch (Exception e) {
            e.printStackTrace();
        }


        var jwtToken = jwtService.generarToken(userDetailsService.loadUserByUsername(usuario.getUsername()));
        return AuthenticationResponse.builder().tipoDeToken("BEARER").tokenDeAcceso(jwtToken).tipoOperacion("Usuario Creado Con exito").build();
    }

    public AuthenticationResponse loginAuthenticated(LoginForm loginForm){
        var usuario = usuarioDao.findByUsername(loginForm.getUsername());
        if (usuario == null) {
            throw new ResourceNotFoundException("User o Password Incorrect", "User", loginForm.getUsername());
        }
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginForm.getUsername(), loginForm.getPassword()));


        var jwtToken = jwtService.generarToken(userDetailsService.loadUserByUsername(usuario.getUsername()));
        return AuthenticationResponse.builder().username(usuario.getUsername()).tipoDeToken("BEARER").tipoOperacion("Inicio de Session exitoso").tokenDeAcceso(jwtToken).build();
    }

    public List<UsuarioRol> addUserRol(Usuario usuario) {
        List<UsuarioRol> usuarioRols = new ArrayList<>();
        Optional<Rol> rolesFind = rolDao.findById(1L);
        if (rolesFind.isPresent()) {
            List<Rol> rolesforUser = rolesFind.stream().toList();
            for (Rol rol : rolesforUser) {
                UsuarioRol userRol = new UsuarioRol();
                userRol.setCodUsuario(usuario);
                userRol.setCodRol(rol);
                usuarioRols.add(userRol);
            }
            return usuarioRols;
        }
        return new ArrayList<>();
    }

}
