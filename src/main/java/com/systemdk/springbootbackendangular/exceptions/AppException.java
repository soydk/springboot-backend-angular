package com.systemdk.springbootbackendangular.exceptions;

import org.springframework.http.HttpStatus;

public class AppException extends RuntimeException{

    private static final Long serialVersionUID = 1L;

    private HttpStatus estado;
    private String mensaje;

    public AppException(HttpStatus estado, String mensaje) {
        this.estado = estado;
        this.mensaje = mensaje;
    }

    public HttpStatus getEstado() {
        return estado;
    }

    public void setEstado(HttpStatus estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
