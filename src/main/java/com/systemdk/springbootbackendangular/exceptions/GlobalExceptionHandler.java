package com.systemdk.springbootbackendangular.exceptions;

import com.systemdk.springbootbackendangular.dto.ErrorDetalleDTO;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;


@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)//maneja excepciones de recursos no encontrados
    public ResponseEntity<ErrorDetalleDTO> manejarResourceNotFountExceptions(ResourceNotFoundException exception, WebRequest webRequest) {
        ErrorDetalleDTO errorDetalleDTO = new ErrorDetalleDTO(new Date(), exception.getMessage(), webRequest.getDescription(false));
        return new ResponseEntity<>(errorDetalleDTO, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)//maneja excepciones globales de toda la aplicacion
    public ResponseEntity<ErrorDetalleDTO> manejarGlobalException(Exception exception, WebRequest webRequest) {
        ErrorDetalleDTO errorDetalleDTO = new ErrorDetalleDTO(new Date(), exception.getMessage(), webRequest.getDescription(false));
        return new ResponseEntity<>(errorDetalleDTO, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(AuthenticationException.class)//maneja excepciones globales de toda la aplicacion
    public ResponseEntity<ErrorDetalleDTO> manejarGlobalException(AuthenticationException exception, WebRequest webRequest) {
        ErrorDetalleDTO errorDetalleDTO = new ErrorDetalleDTO(new Date(), exception.getMessage(), webRequest.getDescription(false));
        return new ResponseEntity<>(errorDetalleDTO, HttpStatus.UNAUTHORIZED);
    }

    //este metodo maneja cuando los datos no son validos

}
