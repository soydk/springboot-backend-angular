package com.systemdk.springbootbackendangular.utils;

import jakarta.servlet.ServletContext;
import org.springframework.web.jsf.FacesContextUtils;

public class PathFinder {

    public String obtenerPathAbsolute(String tipo){
        return switch (tipo.toUpperCase()) {
            case "IMAGEN" -> "/Users/darwinfernandez/Documents/Code/angular-app/docUpload/imagePerfil";
            case "DOCUMENTOS" -> "/Users/darwinfernandez/Documents/Code/angular-app/docUpload/documents";
            case "NO_IMAGEN" -> "src/main/resources/static/img";
            default -> "";
        };
    }

}
